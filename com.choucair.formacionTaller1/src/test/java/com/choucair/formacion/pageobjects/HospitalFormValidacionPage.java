package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class HospitalFormValidacionPage extends PageObject {
	//Registro Doctor
	@FindBy(xpath="//*[@id=\'name\']")
	public WebElementFacade nombre_Requerido;
	@FindBy(xpath="//*[@id=\"last_name\"]")
	public WebElementFacade apellido_Requerido;
	@FindBy(xpath="//*[@id=\"telephone\"]")
	public WebElementFacade telefono;
	@FindBy(xpath="//*[@id=\"identification_type\"]")
	public WebElementFacade Multi_Selector_Documento;
	@FindBy(xpath="//*[@id=\"identification\"]")
	public WebElementFacade Num_Documento;
	@FindBy(xpath="//*[@id=\"page-wrapper\"]/div/div[3]/div/a")
	public WebElementFacade btnGuardar;
	@FindBy(xpath="//*[@id=\'wrapper\']/nav/div/a")
	public WebElementFacade btnInicio;
	//Registro Paciente 
	@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[1]/input")
	public WebElementFacade nombre_Requerido_Paciente;
	@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[2]/input")
	public WebElementFacade apellido_Requerido_Paciente;
	@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[3]/input")
	public WebElementFacade telefono_Paciente;
	@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[4]/select")
	public WebElementFacade Multi_Selector_Documento_Paciente;
	@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[5]/input")
	public WebElementFacade Num_Documento_Paciente;
	@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[6]/label/input")
	public WebElementFacade Salud_Prepagada;
	@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/a")
	public WebElementFacade btnGuardar_Paciente;
	//Agendar Cita 
	@FindBy(xpath="//*[@id=\'datepicker\']")
	public WebElementFacade Fecha_Cita;
	@FindBy(xpath="//*[@id=\'page-wrapper\']")
	public WebElementFacade Cerrar_Calendario;

	@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[2]/input")
	public WebElementFacade Documento_Paciente;
	@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[3]/input")
	public WebElementFacade Documento_Doctor;
	@FindBy(xpath="//*[@id=\"page-wrapper\"]/div/div[3]/div/div[4]/textarea")
	public WebElementFacade Observaciones;
	@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/a")
	public WebElementFacade btnGuardarCita;
	
	//Metodos formulario Doctor 
	public void Nombre_Requerido(String datoPrueba){
		nombre_Requerido.click();
		nombre_Requerido.clear();
		nombre_Requerido.sendKeys(datoPrueba);
	}
	public void Apellido_Requerido(String datoPrueba){
		apellido_Requerido.click();
		apellido_Requerido.clear();
		apellido_Requerido.sendKeys(datoPrueba);
	}
	public void Telefono(String datoPrueba){
		telefono.click();
		telefono.clear();
		telefono.sendKeys(datoPrueba);
	}
	public void Selector_Documento(String datoPrueba){
		Multi_Selector_Documento.selectByVisibleText(datoPrueba);
	}
	public void Numero_Documento(String datoPrueba){
		Num_Documento.click();
		Num_Documento.clear();
		Num_Documento.sendKeys(datoPrueba);
	}
	public void GardarDoctor(){
		btnGuardar.click();
	}
	
	public void Inicio(){
		btnInicio.click();
	}
	//Metodos formulario Paciente 
	public void Nombre_Requerido_Paciente(String datoPrueba){
		nombre_Requerido_Paciente.click();
		nombre_Requerido_Paciente.clear();
		nombre_Requerido_Paciente.sendKeys(datoPrueba);
	}
	
	public void Apellido_Requerido_Paciente(String datoPrueba){
		apellido_Requerido_Paciente.click();
		apellido_Requerido_Paciente.clear();
		apellido_Requerido_Paciente.sendKeys(datoPrueba);
	}
	public void Telefono_Paciente(String datoPrueba){
		telefono_Paciente.click();
		telefono_Paciente.clear();
		telefono_Paciente.sendKeys(datoPrueba);
	}
	public void Selector_Documento_Paciente(String datoPrueba){
		Multi_Selector_Documento_Paciente.selectByVisibleText(datoPrueba);
	}
	public void Numero_Documento_Paciente(String datoPrueba){
		Num_Documento_Paciente.click();
		Num_Documento_Paciente.clear();
		Num_Documento_Paciente.sendKeys(datoPrueba);
	}
	public void Salud_Prepagada(){	
		Salud_Prepagada.click();
	}
	
	public void GardarPaciente(){
		btnGuardar.click();
	}
     // Agendar Cita 
	public void Fecha_Cita(String datoPrueba){
	Fecha_Cita.click();
		Fecha_Cita.clear();
		Fecha_Cita.sendKeys(datoPrueba);
		//waitFor(5).second();
		//Fecha_Cita.click();

		
	}
	public void Cerrar_Calendario(){		
			Cerrar_Calendario.click();
		}
	
	public void Docum_Paciente(String datoPrueba){
		Documento_Paciente.click();
		Documento_Paciente.clear();
		Documento_Paciente.sendKeys(datoPrueba);
	}
	public void Docum_Doctor(String datoPrueba){
		Documento_Doctor.click();
		Documento_Doctor.clear();
		Documento_Doctor.sendKeys(datoPrueba);
	}
	public void Observaciones(String datoPrueba){
		Observaciones.click();
		Observaciones.clear();
		Observaciones.sendKeys(datoPrueba);
	}
	public void GardarCita(){
		btnGuardarCita.click();
	}
}
