package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.pageobjects.RegistroDoctor;
import com.choucair.formacion.steps.HospialFormValidacioSteps;
import com.choucair.formacion.steps.PopupValidacionSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;

public class PopupValidacionDefinicion {
	
	@Steps
	PopupValidacionSteps PopupValidacionSteps;
	@Steps
	HospialFormValidacioSteps HospialFormValidacionSteps;

	//Doctor
	@Given("^Ingresar al aplicativo dministración de Hospitales$")
	public void ingresar_al_aplicativo_dministración_de_Hospitales()  {
		PopupValidacionSteps.ingresar_al_aplicativo_administracion_de_hospitales();
	    
	}


	@When("^Realiza el registro del mismo en el aplicativo de Administración de Hospitales$")
	public void realiza_el_registro_del_mismo_en_el_aplicativo_de_Administración_de_Hospitales(DataTable dtDatosForm){
		  List<List<String>> data = dtDatosForm.raw();
			
			for (int i = 1; i < data.size(); i++) {
				HospialFormValidacionSteps.diligenciar_popup_datos_tabla(data, i);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
				
				}
			}
	  
	}

	@Then("^el verifica que se presente en pantalla el mensaje Datos guardados correctamente$")
	public void el_verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente(){
		try {
			HospialFormValidacionSteps.verificar_ingreso_datos_formulario_exitoso();
			Thread.sleep(4000);
		} catch (InterruptedException e) {
		
		}
	}
//Paciente
	@Given("^Ingresar al aplicativo dministración de Hospitales2$")
	public void ingresar_al_aplicativo_dministración_de_Hospitales2()  {
		PopupValidacionSteps.ingresar_al_aplicativo_administracion_de_hospitales2();
	    
	}
	@When("^el realiza el registro del mismo en el aplicativo de Administración de Hospitales2$")
	public void realiza_el_registro_del_mismo_en_el_aplicativo_de_Administración_de_Hospitales2(DataTable dtDatosForm){
		  List<List<String>> data = dtDatosForm.raw();
			
			for (int i = 1; i < data.size(); i++) {
				HospialFormValidacionSteps.diligenciar_popup_datos_tabla2(data, i);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
				
				}
			}	  
	}
	@Then("^el verifica que se presente en pantalla el mensaje Datos guardados correctamente2$")
	public void el_verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente2(){
		try {
			HospialFormValidacionSteps.verificar_ingreso_datos_formulario_exitoso2();
			Thread.sleep(4000);
		} catch (InterruptedException e) {
		
		}
	}
	
	//Agendar cita 
	
	@Given("^Carlos necesita asistir al medico$")
	public void carlos_necesita_asistir_al_medico()  {
   PopupValidacionSteps.ingresar_al_aplicativo_administracion_de_hospitales3();

	}


	@When("^el realiza el agendamiento de una Cita$")
	public void el_realiza_el_agendamiento_de_una_Cita(DataTable dtDatosForm) { 
	List<List<String>> data = dtDatosForm.raw();
	
	for (int i = 1; i < data.size(); i++) {
		HospialFormValidacionSteps.diligenciar_popup_datos_tabla3(data, i);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		
		}
	}

}
	@Then("^el verifica que se presente en pantalla el mensaje Datos guardados correctamente3$")
	public void el_verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente3(){
		try {
			HospialFormValidacionSteps.verificar_ingreso_datos_formulario_exitoso3();
			Thread.sleep(4000);
		} catch (InterruptedException e) {
		
		}
	}
	
}
