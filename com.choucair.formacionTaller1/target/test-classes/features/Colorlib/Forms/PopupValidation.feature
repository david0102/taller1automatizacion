
@Regresion
Feature: Gestionar Cita Médica
  Como paciente
  Quiero realizar la solicitud de una cita médica
  A través del sistema de Administración de Hospitales


@CasoExitosoRegistroDoctor
	Scenario:Realizar el Registro de un Doctor.
	Given Ingresar al aplicativo dministración de Hospitales
	When  Realiza el registro del mismo en el aplicativo de Administración de Hospitales
	|Nombre_completo|Apellidos|Teléfono|Tipo_de_documento_de_identidad|Documento_de_identidad|
	|Javier           |Hernandez |5702323 |Pasaportes                    |9876543210           |
	Then el verifica que se presente en pantalla el mensaje Datos guardados correctamente
	
@CasoExitosoRegistroPaciente
	Scenario:Realizar el Registro de Paciente.
	Given Ingresar al aplicativo dministración de Hospitales2
	When  el realiza el registro del mismo en el aplicativo de Administración de Hospitales2
	|Nombre_completo|Apellidos|Teléfono|Tipo_de_documento_de_identidad|Documento_de_identidad|Salud Prepagada|
	|Jose           |Peres |7890987 |Cédula de ciudadanía         |1234567890            |x              |
	Then el verifica que se presente en pantalla el mensaje Datos guardados correctamente2
	
@CasoExitosoAgendamientoCita
Scenario:Realizar el Agendamiento de una Cita.
	Given Carlos necesita asistir al medico
	When  el realiza el agendamiento de una Cita
	|Día_de_la_cita|Documento_de_identidad_del_paciente|Documento_de_identidad_del_doctor|Observaciones      |
	|03/07/2019    |1234567890                          |9876543210                        |Cita Medico General|
	Then el verifica que se presente en pantalla el mensaje Datos guardados correctamente3

	